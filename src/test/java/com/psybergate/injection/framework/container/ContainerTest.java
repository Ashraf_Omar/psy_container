package com.psybergate.injection.framework.container;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.psybergate.injection.client.consumer.Consumer;
import com.psybergate.injection.framework.container.impl.ContainerImpl;

/**
 * @author ashraf.omar
 * @since 23 Jul 2020
 */
public class ContainerTest {

  private Container injector;

  @Before
  public void init() {
    injector = new ContainerImpl();
  }

  @Test
  public void testBasicConstructionOfObjects() {
    Consumer consumer = injector.getInstanceOf(Consumer.class);
    assertNotNull(consumer);
  }

  @Test
  public void testDifferentBasicObjectsAreConstructed() {
    Consumer unexpected = injector.getInstanceOf(Consumer.class);
    Consumer actual = injector.getInstanceOf(Consumer.class);
    assertNotSame(unexpected, actual);
  }

  @Test
  public void testInjection() {
    Consumer consumer = injector.getInstanceOf(Consumer.class);
    consumer.processMessages("Hi Ashraf", "ashraf@mail.com");
  }

  @Test
  public void testDifferentObjectsUsedInInjection() {
    Consumer unexpected = injector.getInstanceOf(Consumer.class);
    Consumer actual = injector.getInstanceOf(Consumer.class);
    assertNotSame(unexpected.getService(), actual.getService());
  }

}
