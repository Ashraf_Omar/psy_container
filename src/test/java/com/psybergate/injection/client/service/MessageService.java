package com.psybergate.injection.client.service;

public interface MessageService {

  void sendMessage(String msg, String rec);
}