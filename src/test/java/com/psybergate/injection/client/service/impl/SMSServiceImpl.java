package com.psybergate.injection.client.service.impl;

import com.psybergate.injection.client.service.MessageService;

//@MyComponent
public class SMSServiceImpl
    implements MessageService {

  @Override
  public void sendMessage(String msg, String rec) {
    // logic to send SMS
    System.out.println("SMS sent to " + rec + " with Message=" + msg);
  }

}