package com.psybergate.injection.client.service.impl;

import com.psybergate.injection.client.service.MessageService;
import com.psybergate.injection.framework.annotation.MyComponent;

@MyComponent
public class EmailServiceImpl
    implements MessageService {

  @Override
  public void sendMessage(String msg, String rec) {
    // logic to send email
    System.out.println("Email sent to " + rec + " with Message=" + msg);
  }

}