package com.psybergate.injection.client.consumer;

import com.psybergate.injection.client.service.MessageService;

public interface Consumer {

  void processMessages(String msg, String rec);

  MessageService getService();

}