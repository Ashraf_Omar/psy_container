package com.psybergate.injection.client.consumer;

import com.psybergate.injection.client.service.MessageService;
import com.psybergate.injection.framework.annotation.MyComponent;
import com.psybergate.injection.framework.annotation.MyInject;

@MyComponent
public class MyDIApplication
    implements Consumer {

  private MessageService service;

  @MyInject
  public MyDIApplication(MessageService svc) {
    this.service = svc;
  }

  @Override
  public void processMessages(String msg, String rec) {
    // do some msg validation, manipulation logic etc
    this.service.sendMessage(msg, rec);
  }

  public MessageService getService() {
    return service;
  }

  public void setService(MessageService service) {
    this.service = service;
  }

}