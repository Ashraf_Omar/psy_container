package com.psybergate.injection.framework.container.impl;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.reflections.Reflections;

import com.psybergate.injection.framework.annotation.MyComponent;
import com.psybergate.injection.framework.annotation.MyInject;
import com.psybergate.injection.framework.container.Container;
import com.psybergate.injection.framework.factory.Factory;
import com.psybergate.injection.framework.factory.impl.ReflectiveFactory;

/**
 * @author ashraf.omar
 * @since 23 Jul 2020
 */
public class ContainerImpl
    implements Container {

  /**
   * Reflections: Open-source library for scanning classpath. The String in the constructor is the package name. Blank
   * means this package and all sub-packages.
   */
  public static final Reflections reflections = new Reflections("");

  private final Map<Class<?>, Factory<?>> contextMap = new HashMap<>();

  public ContainerImpl() {
    scanClassPathAndRegisterComponents();
  }

  private void scanClassPathAndRegisterComponents() {
    Set<Class<?>> components = reflections.getTypesAnnotatedWith(MyComponent.class);
    for (Class<?> component : components) {
      for (Class<?> thInterface : component.getInterfaces()) {
        final Factory<?> factory = createFactoryFor(component);
        registerFactory(thInterface, factory);
      }
    }
  }

  private void registerFactory(Class<?> iface, final Factory<?> factory) {
    contextMap.put(iface, factory);
  }

  @SuppressWarnings("unchecked")
  private <T> Factory<T> createFactoryFor(Class<T> component) {
    if (component.isInterface()) {
      final Set<Class<? extends T>> subTypes = reflections.getSubTypesOf(component);
      for (Class<? extends T> subtype : subTypes) {
        if (subtype.isAnnotationPresent(MyComponent.class)) {
          component = (Class<T>) subtype;
          break;
        }
        else {
          component = (Class<T>) subTypes.toArray()[0];// Just get the first impl you can find
        }
      }
    }
    Constructor<T> constructor = getConstructorFor(component);
    List<Factory<?>> dependencies = getFactoriesForDependencies(constructor);
    return new ReflectiveFactory<T>((Constructor<T>) constructor, dependencies);
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T> T getInstanceOf(Class<?> id) {
    Factory<T> factory = (Factory<T>) contextMap.get(id);
    return factory.getInstance();
  }

  @SuppressWarnings("unchecked")
  private <T> Constructor<T> getConstructorFor(final Class<?> component)
      throws SecurityException {
    Constructor<?>[] constructors = component.getConstructors();
    for (Constructor<?> constructor : constructors) {
      if (constructor.isAnnotationPresent(MyInject.class)) {
        return (Constructor<T>) constructor;
      }
    }
    try {
      return (Constructor<T>) component.getConstructor();
    }
    catch (NoSuchMethodException ex) {
      throw new RuntimeException("Error - No args constructor not declared: " + component.getName(),
          ex);
    }
    catch (SecurityException ex) {
      throw new RuntimeException("Error - No args constructor is private: " + component.getName(),
          ex);
    }
  }

  private List<Factory<?>> getFactoriesForDependencies(Constructor<?> constructor) {
    Class<?>[] parameters = constructor.getParameterTypes();
    List<Factory<?>> dependencies = new ArrayList<>();
    for (Class<?> dependency : parameters) {
      Factory<?> factory = contextMap.get(dependency);
      if (factory == null) {
        factory = createFactoryFor(dependency);
        registerFactory(dependency, factory);
      }
      dependencies.add(factory);
    }
    return dependencies;
  }

  @Override
  public boolean containsInstanceOf(Class<?> id) {
    return contextMap.containsKey(id);
  }

}
