package com.psybergate.injection.framework.container;

/**
 * @author ashraf.omar
 * @since 23 Jul 2020
 */
public interface Container {

  /**
   * Gets an object of a given class/type
   * 
   */
  <T> T getInstanceOf(Class<?> id);

  /**
   * Checks if the class is in the container.
   */
  boolean containsInstanceOf(Class<?> id);

}
