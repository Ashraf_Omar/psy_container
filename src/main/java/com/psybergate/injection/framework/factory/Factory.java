package com.psybergate.injection.framework.factory;

/**
 * @author ashraf.omar
 * @since 22 Jul 2020
 */
public interface Factory<T> {

  T getInstance();

}
