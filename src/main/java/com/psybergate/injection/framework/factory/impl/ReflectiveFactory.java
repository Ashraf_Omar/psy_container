package com.psybergate.injection.framework.factory.impl;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import com.psybergate.injection.framework.factory.Factory;

/**
 * @author ashraf.omar
 * @since 23 Jul 2020
 */
public class ReflectiveFactory<T>
    implements Factory<T> {

  Constructor<T> constructor;

  private List<Factory<?>> dependencies;

  public ReflectiveFactory(Constructor<T> constructor, List<Factory<?>> dependencies) {
    this.constructor = constructor;
    this.dependencies = dependencies;
  }

  @Override
  public T getInstance() {
    try {
      Object[] initargs = getInitArgs();
      if (dependencies.size() != 0) {
        return this.constructor.newInstance(initargs);
      }
      else {
        return this.constructor.newInstance();
      }
    }
    catch (InstantiationException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException ex) {
      throw new RuntimeException(
          "Error - Could not instantiate: " + constructor.getDeclaringClass(), ex);
    }
  }

  private Object[] getInitArgs() {
    List<Object> initargs = new ArrayList<>();
    for (Factory<?> factory : dependencies) {
      initargs.add(factory.getInstance());
    }
    return initargs.toArray();
  }

}
