package com.psybergate.injection.framework.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author ashraf.omar
 * @since 23 Jul 2020
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface MyInject {

}
